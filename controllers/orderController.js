const Product = require("../models/productModel")
const Order = require("../models/orderModel")

/*
order status [
    'to ship', order is created waiting for seller to ship
    'to receive', order is on shipping line
    'cancelled', order is cancelled by user or seller
    'completed', smooth transaction up to delivery
]
*/

module.exports.updateStocks = async (product) => {    
    return Product.updateOne(product._id, {
        _id: product._id ,
        $inc: { stock: -product.quantity} 
   }).then((result, error) => error ? result : true )
}

const loadProductPrice = async (products) => {    
    let res;
    try {
        res = await Product.find({
            _id: { $in: products.map(e=>e.productId) }
         },{price: 1})
         .then(result => result)
         .catch(error => error)
    }finally{return res}
}

module.exports.createOrder = async (userId, body) => {     
    let p = await loadProductPrice(body.products)      
        
    body.products.forEach((product,i) => product.price = p[i].price )
    
    let newOrder = new Order({
        orderedBy: userId,
        products: body.products,
        totalPrice: body.products.reduce((c,p) => c + (p.price * p.quantity),0),
        totalQuantity: body.products.reduce((c,p) => c + p.quantity,0)
    })
        
    return newOrder.save().then((product, error) => error ? false : true) 
}

module.exports.viewMyOrder = (userId) => {
    return Order.find({orderedBy: userId}, {
        "orderedBy": 1,
        "products": 1,
        "totalPrice": 1,
        "totalQuantity": 1,
        "status": 1
      }).then(result => result).catch(error => error)
}

module.exports.viewMyOrderByStatus = (userId, str) => {    
    return Order.find({orderedBy: userId, status: str}, {
        "orderedBy": 1,
        "products": 1,
        "totalPrice": 1,
        "totalQuantity": 1,
        "status": 1
      }).then(result => result).catch(error => error)
}

module.exports.viewAllOrder = () => {
    return Order.find({isAdmin: true}, {
        "products": 1,
        "totalPrice": 1,
        "totalQuantity": 1,
        "status": 1,
        "orderedBy": 1
      }).then(result => result).catch(error => error)
}

module.exports.viewAllOrderByStatus = (str) => {
    return Order.find({isAdmin: true, status: str}, {
        "products": 1,
        "totalPrice": 1,
        "totalQuantity": 1,
        "status": 1,
        "orderedBy": 1
      }).then(result => result).catch(error => error)
}