const Product = require("../models/productModel");

module.exports.getProducts = () => {
  return Product.find({ isActive: true }).then((result) => result);
};

module.exports.searchProduct = (str) => {   
  return Product
  .find({ $text: { $search: str , $caseSensitive: false} },
    {
    _id: 0,
    productName: 1,
    description:1,
    color:1,
    category:1,
    rating: 1,
    stock: 1
  }).then(res => res )
};

module.exports.addProduct = (body) => {
  let newProduct = new Product({
    productName: body.productName,
    description: body.description,
    category: body.category,
    color: body.color,
    price: body.price,
    rating: body.rating,
    stock: body.stock,
  });

  return newProduct.save().then((product, error) => (error ? false : true));
};

// Update Product by Admin Only
module.exports.updateProduct = (productId, body) => {
  let updatedProduct = {
    productName: body.productName,
    description: body.description,
    category: body.category,
    color: body.color,
    price: body.price,
    rating: body.rating,
    stock: body.stock,
  };

  return Product.findByIdAndUpdate(productId, updatedProduct).then(
    (course, error) => (error ? false : true)
  );
};

// Get product Details
module.exports.getProductDetails = (productId) => {
  return Product.findById(productId).then((result) => result);
};

module.exports.archiveProduct = (productId, body) => {
  let updatedProduct = { isActive: false };
  return Product.findByIdAndUpdate(productId, updatedProduct).then(
    (course, error) => (error ? false : true)
  );
};
