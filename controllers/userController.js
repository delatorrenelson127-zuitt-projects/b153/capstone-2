const User = require('../models/userModel')
// const Product = require('../models/productModel')
// const Order = require('../models/orderModel')

const bcrypt = require('bcrypt')
const auth = require('../auth')
// const res = require('express/lib/response')


// User registration
module.exports.register = (body) => {   
    return User.find({email: body.email}).then(result => {
        if(result.length > 0) {
            console.log('Email already Exists')            
        }else{
            console.log('Email Not Exist, continue register')
            let newUser = new User({
                firstName: body.firstName,
                lastName: body.lastName,
                email: body.email,
                address: body.address,        
                password: bcrypt.hashSync(body.password,10),
                mobileNo: body.mobileNo
            })        
            return newUser.save().then((user, error) => error ? false : console.log('Registered Successfully'))                   
        }
    })  
}

module.exports.login = (body) => {
    return User.findOne({email: body.email}).then(result =>{
        if(result === null){
            return false
        }else{
            // returns a Boolean Value via bcrypt
            const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
            return isPasswordCorrect ? {accessToken: auth.createAccessToken(result.toObject())} : false;
        }
    })
}

module.exports.getProfile = (userId) => {
    return User.findById(userId).then(result => {
        result.password = undefined 
        return result;
    })
}