const express = require('express')
const mongoose = require('mongoose')

// Router
const productsRoutes = require('./routes/productRoute')
const userRoutes = require('./routes/userRoute')
const orderRoutes = require('./routes/orderRoute')

const username = 'admin';
const password = 'admin';
const db = 'ecommerce'

// const port = 27017
// mongoose.connect(`mongodb://localhost:${port}/${db}`,{useNewUrlParser: true,useUnifiedTopology: true})

const port = 3000
mongoose.connect(
    `mongodb+srv://${username}:${password}@testclusterforrobo3t.hpoil.mongodb.net/${db}?retryWrites=true&w=majority`,
    {useNewUrlParser: true,useUnifiedTopology: true}
)

mongoose.connection.once('open', () => console.log('Now connected to Server'))

const app = express()

// middlewear that allows our app to receive nested JSON data
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/products', productsRoutes)
app.use('/users', userRoutes)
app.use('/order', orderRoutes)

app.listen(process.env.PORT || port, () =>{console.log(`Server running on port ${port}`)})