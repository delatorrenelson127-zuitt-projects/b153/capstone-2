const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required : [true, 'Product name is required.']
    },
    description: {
        type: String,
        required: [true, 'Product description is required.']
    },
    price: {
        type: Number,
        required: [true, 'Price is required.']
    },
    category: {
        type: String,
        required: [true, 'Category is required.']
    },
    color: {
        type: String,
        required: [true, 'Color is required.']
    },    
    imageURL: {
        type: String,
        default: ""
    },
    stock: {
        type: Number,
        default: 0,
    },
    rating: {
        type: Number,
        default: 0,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Products', productSchema)