// User Model
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required."],
  },
  lastName: {
    type: String,
    required: [true, "Last name is required."],
  },
  email: {
    type: String,
    require: [true, "Email is required."],
  },
  password: {
    type: String,
    required: [true, "Password is required."],
  },
  address: [
    {
      country: {
        type: String,
        required: [true, "Country is required."],
      },
      addressLine: {
        type: String,
        required: [true, "This field is required."],
      },
      city: {
        type: String,
        required: [true, "City is required."],
      },
      zipcode: {
        type: Number,
        required: [true, "Zip Code is required."],
      },
    },
  ],
  mobileNo: {
    type: String,
    required: [true, "Mobile number is required."],
  },
  isAdmin: { type: Boolean, default: false },
  isActive: { type: Boolean, default: true },
});

module.exports = mongoose.model("User", userSchema);

/* User Input Testing
{
    "firstName": "Admin1firstName",
    "lastName": "Admin1lastName",
    "email": "Admin1@mail.com",
    "password": "Admin1Password",
    "mobileNo": "12345678901"
}

{
    "firstName": "user2firstName",
    "lastName": "user2lastName",
    "email": "user2@mail.com",
    "password": "user2Password",
    "mobileNo": "12345678901",
}
*/
