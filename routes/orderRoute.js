const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth')

router.post('/checkout',auth.verify, (req, res)=>{    
    const userId = auth.decode(req.headers.authorization).id;
    orderController
    .createOrder(userId, req.body)
    .then(resultFromController => {
        req.body.products.forEach(product => orderController.updateStocks(product))
        res.send(resultFromController)
    })   
})

router.get('/myorder',auth.verify, (req, res)=>{    
    const userId = auth.decode(req.headers.authorization).id;
    orderController
    .viewMyOrder(userId)
    .then(resultFromController => res.send(resultFromController))   
})

router.get('/myorder/:myOrderStatus',auth.verify, (req, res)=>{    
    const userId = auth.decode(req.headers.authorization).id;
    orderController
    .viewMyOrderByStatus(userId, Object.values(req.params)[0])
    .then(resultFromController => res.send(resultFromController))   
})


//Admin Only
router.get('/orders',auth.verify, (req, res)=>{    
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        orderController
        .viewAllOrder()
        .then(resultFromController => res.send(resultFromController))
    }else{ res.send('Non Admin')}           
})

router.get('/orders/:OrderStatus',auth.verify, (req, res)=>{    
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        orderController
        .viewAllOrderByStatus(Object.values(req.params)[0])
        .then(resultFromController => res.send(resultFromController))
    }else{ res.send('Non Admin')}           
})


module.exports = router;