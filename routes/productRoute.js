const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')


// Get all Products
router.get('/', (req, res)=>{
    productController
    .getProducts()
    .then(resultFromController => res.send(resultFromController))
})

// Get product Details
router.get('/:productId', (req, res)=>{    
    productController
    .getProductDetails(Object.values(req.params)[0])
    .then(resultFromController => res.send(resultFromController))
})

// Product Search
router.get('/q/:keywords', (req, res)=>{    
    productController
    .searchProduct(Object.values(req.params)[0])
    .then(resultFromController => res.send(resultFromController))
})

// Update Product by Admin Only
router.put('/:productId', auth.verify, (req, res) =>{
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        productController
        .updateProduct(req.params.productId, req.body)
        .then(resultFromController => {res.send(resultFromController)})
    }else{ res.send('Non Admin')}   
})


// Add Product no Authentication yet
router.post('/',  (req,res)=>{    
    productController
    .addProduct(req.body)
    .then(resultFromController => res.send(resultFromController)) 
})

// Product Archiving by Admin Only
router.delete('/:productId', auth.verify, (req, res) =>{
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        productController
        .archiveProduct(req.params.productId, req.body)
        .then(resultFromController => res.send(resultFromController))
    }else{ res.send('Non Admin')}     
})


module.exports = router;