const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


router.post('/register', (req, res)=>{
    userController
    .register(req.body)
    .then(resultFromController => res.send(resultFromController))   
})


router.get('/login', (req, res)=>{    
    userController
    .login(req.body)
    .then(resultFromController => res.send(resultFromController))
})


router.get('/details', auth.verify, (req,res)=>{
    const userId = auth.decode(req.headers.authorization).id;
    userController
    .getProfile(userId)
    .then(resultFromController => res.send(resultFromController))    
})

module.exports = router;